# Table of Contents


[[_TOC_]]

## Morello containers

Morello CI/CD uses containers located in the [Morello-ci-containers repo](https://git.morello-project.org/morello/morello-ci-containers)

## Morello pipelines
Morello pipelines are located in [Morello-ci-pipelines repo](https://git.morello-project.org/morello/morello-ci-pipelines).
To enable a repository to use a specific pipeline, configure the ci/cd config file in the repo setting to point to the desired pipeline. More info on gitlab docs [here](https://docs.gitlab.com/ee/ci/pipelines/settings.html#specify-a-custom-cicd-configuration-file). 

The pipelines are:
### firmware
- [edk2(UEFI)](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/master/.gitlab-ci-edk2.yml) 
- [System Control Processor](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/master/.gitlab-ci-scp.yml)
- [TFA](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/master/.gitlab-ci-tf-a.yml)

### android
- [android](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/master/.gitlab-ci-android.yml)
- [gdb](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/master/.gitlab-ci-gdb.yml)

### toolchain
- [TC](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/master/.gitlab-ci-toolchain.yml)

### Other
- [Nightly scheduled sanity build](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/master/.gitlab-ci.yml)
- [User triggered build for related merge requests](https://git.morello-project.org/morello/morello-ci-pipelines/-/blob/master/.gitlab-ci-poll-trigger.yml)

## Morello pipelines features
### 1. Support for patchsets across multiple repositories
Users can use labels to earmark a group of related merge requests (across different repos) to enable triggering a pipeline to build them. The [morello-ci-trigger](https://git.morello-project.org/morello/morello-ci-trigger) project runs a scheduled pipeline that polls for **"trigger-pipeline"** label along with label with a prefix **"group-"** on any MR and triggers the pipeline on **manifest** project with details of all the MRs that have same label "group-*".
#### Workflow
1. Create and add a "group-\<feature\>" label across all the merge requests that are interdependent.
2. Once ready for testing, Add the label "trigger-pipeline" on one of the MR.
3. A pipeline is run on manifest project and a label "triggered" is posted on all the MR.
#### Notes/precautions:
  - Only create labels in the form of "group-*" to group the merge requests.
  - Once pipeline is triggered,a "triggered" label is added to the MR.
  - If user decides to retrigger a pipeline, it is recommended to delete the "triggered" label that is added automatically and then recreate the "trigger-pipeline" label again.
  - If another MR is later decided to be included to the same group after the pipeline has been triggered for a set of merge requests, then either:

      1. terminate the already triggered pipeline, add the same "group-*" label to the MR, delete the "triggered" label from the MR where "trigger-pipeline" was previously added and recreate another "trigger-pipeline" label and wait for the scheduled trigger job to kick it or
      2. wait for the triggered pipeline to finish, tag the new MR with the group label then delete the "triggered" label and recreate "trigger-pipeline" label again on any MR from the set.
  - label color is not relevant
  - creating so many label will clutter the dropdown for labels on the project. This needs to be maintained by the user, otherwise there will be too many labels hanging around.
##### **Example**
- The link to the pipeline is posted on all the MR back as shown below.
    * https://git.morello-project.org/morello/morello-ci-containers/-/merge_requests/8#note_10368
- Once the pipeline finishes, the pipeline Success/Failure is posted back as a comment on all the MR back as shown below.
    * https://git.morello-project.org/morello/morello-ci-containers/-/merge_requests/8#note_10450

### 2.User defined tests
Users can selectively specify a certain test(s) to be run for a pipeline, however the pipeline must be manually triggered by the user by going to CI/CD --> pipelines on the left side menu on the screen and clicking "run pipeline" button. Once clicked user will see this [view](https://git.morello-project.org/morello/morello-ci-pipelines/-/pipelines/new)
 They should then specify **'USER_DEFINED_TESTS'** as the _"input variable key"_ and specify a space separated string with the desired tests names in the _"input variable value"_ like so:
```
android-binder android-compartment android-device-tree android-dvfs
android-logd android-multicore android-bionic
```
Then clicking "Run pipeline" will trigger a pipeline that will run only the tests specified by the user.

### 3.Dynamic children pipelines
Tests for the test stage in all pipelines are dynamically generated and run simultaneously. This helps long running tests like bionic to be broken into smaller tests each running in its own child pipeline. Currently this is enabled for all android and bionic tests.


### 4.Submitting tests to LAVA for validation
All tests are submitted to the [morello LAVA instance](https://lava.morello-project.org/) for validation.


## Useful References

### gitlab pipelines
- [Understanding gitlab pipelines](https://docs.gitlab.com/ee/ci/yaml/index.html)
- [How to set pipeline config file for a repository](https://docs.gitlab.com/ee/ci/pipelines/settings.html#specify-a-custom-cicd-configuration-file)
- [List of gitlab predefined variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [Parent Child pipelines](https://docs.gitlab.com/ee/ci/pipelines/parent_child_pipelines.html)

### lava documention
- [Understanding LAVA timeouts](https://lava.morello-project.org/static/docs/v2/timeouts.html?highlight=timeout#timeouts)
